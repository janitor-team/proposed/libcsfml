libcsfml (2.5-1) unstable; urgency=medium

  * New upstream version.
  * Rename all library packages for SONAME bump.
  * Drop patch to allow building on GNU/Hurd.
  * Drop patch to remove commented out googleapi css.

  * d/control:
    - Bump standards version to 4.2.1.
    - Mark libcsfml-doc multi-arch foriegn
  * d/*.symbols: Add new symbols in 2.5.
  * d/libcsfml-doc.doc-base: Fix path to index page.

 -- James Cowgill <jcowgill@debian.org>  Sun, 04 Nov 2018 17:44:54 +0000

libcsfml (2.4-4) unstable; urgency=medium

  [ James Cowgill ]
  * debian/compat: Use debhelper compat 11.
  * debian/control:
    - Use secure Homepage URL.
    - Switch Vcs URLs to salsa.debian.org.
    - Remove Testsuite: autopkgtest field.
    - Set Rules-Requires-Root: no.
    - Bump standards version to 4.1.4.
  * debian/copyright:
    - Update copyright years.
    - Use secure copyright Format URL.
  * debian/libcsfml-doc.docs: Remove debian/tmp/ prefix.
  * debian/libcsfml-doc.doc-base:
    - Fix paths to point to /usr/share/doc/libcsfml-dev.
  * debian/libcsfml-doc.links:
    - Remove jquery symlink.
  * debian/patches:
    - Add patch to use SFML 2.5 CMake config scripts in the build system.
      (Closes: #898913)
  * debian/rules:
    - Use /usr/share/dpkg/architecture.mk to define DEB_HOST_MULTIARCH.
    - Remove now unused CMAKE_MODULE_PATH define from configure override.

  [ Christoph Egger ]
  * Remove myself from uploaders.

 -- James Cowgill <jcowgill@debian.org>  Thu, 17 May 2018 16:57:59 +0100

libcsfml (2.4-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/control:
    - Bump standards to 4.0.0 (no changes required).

 -- James Cowgill <jcowgill@debian.org>  Mon, 19 Jun 2017 10:43:21 +0100

libcsfml (2.4-1) experimental; urgency=medium

  * New upstream version.
    - Bump SFML build dependency to 2.4.
    - Rename packages for SONAME bump.
    - Update symbols files for 2.4.

  * debian/control:
    - Use debhelper compat 10.
    - Run wrap-and-sort -s.
  * debian/rules:
    - Remove obsolete dh_strip override.

 -- James Cowgill <jcowgill@debian.org>  Fri, 03 Mar 2017 23:34:44 +0000

libcsfml (2.3-3) unstable; urgency=medium

  * debian/control:
    - Drop libcsfml2.3-dbg and automatic debug packages instead.
    - Use secure Vcs URLs.
    - Bump standards to 3.9.8.
    - Use my debian.org email address.
  * debian/tests:
    - Add autopkgtests.
  * debian/rules:
    - Drop get-orig-source target - use uscan instead.

 -- James Cowgill <jcowgill@debian.org>  Tue, 28 Jun 2016 21:09:02 +0100

libcsfml (2.3-2) unstable; urgency=medium

  * Upload 2.3 to unstable.

 -- James Cowgill <james410@cowgill.org.uk>  Thu, 16 Jul 2015 18:27:20 +0100

libcsfml (2.3-1) experimental; urgency=medium

  * New upstream version.
  * Rename packages to libcsfml-*2.3 due to SONAME bump.
  * Refresh patches and symbols for new version.

  * debian/control:
    - Undo debug package rename (has more downsides than advantages).
    - Bump required SFML version to 2.3.
    - Use jquery from libjs-query in libcsfml-doc.
    - Use cgit Vcs-Browser URL.
  * debian/watch:
    - New upstream GitHub URL.

 -- James Cowgill <james410@cowgill.org.uk>  Wed, 10 Jun 2015 00:32:33 +0100

libcsfml (2.2-1) experimental; urgency=low

  * New upstream version.
  * Rename packages to libcsfml-*2.2 due to SONAME bump.

  * debian/control:
    - Fix capitalization of "Debian Games Team".
    - Rename libcsfml2-dbg to libcsfml-dbg.
    - Add some suggests to libcsfml-dev.
    - Bump required SFML version to 2.2
    - Bump standards version - no changes
  * debian/copyright:
    - Update dates and remove copyright for the cmake module patch.
  * debian/rules:
    - Use uscan --download-current-version.
    - Update cmake options (some were renamed).
    - Enable all hardening flags.
  * debian/patches:
    - Refresh hurd patch and add 01_ prefix.
    - Drop sfml-cmake-module.patch and use FindSFML from libsfml-dev instead.
    - Add 02_build-doc-once.patch to prevent documentation building twice.
    - Add 03_remove-googleapi-css.patch to remove googleapi css in
      doc/header.htm.
  * debian/*.symbols:
    - Update all symbol files for 2.2
    - Add Build-Depends-Package field.

 -- James Cowgill <james410@cowgill.org.uk>  Sat, 14 Mar 2015 15:57:23 +0000

libcsfml (2.1-2) unstable; urgency=low

  * Fix Vcs-Browser field in debian/control

 -- James Cowgill <james410@cowgill.org.uk>  Mon, 16 Dec 2013 15:24:15 +0000

libcsfml (2.1-1) unstable; urgency=low

  * New upstream release 2.1
  * Added myself to the list of uploaders
  * Change package priority from extra to optional
  * Add dependency to libgl-dev and libglu-dev to libcsfml-dev
  * Modify debian/copyright to use the machine-readable format
    (http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/)
  * Added doc-base file for API documentation
  * Moved debian/watch source to the CSFML github repository
  * Upgrade source format to 3.0 and debhelper 9 using dh
  * Upgrade to standards version 3.9.5
  * Updated package description to fix typos (Closes: #704462)
  * Added multiarch support
  * Added symbols files for the libraries
  * Removed old conflicts on libcsfml1.deb3-dev
  * Added lintian override hardening-no-fortify-functions for
    libcsfml-network2

 -- James Cowgill <james410@cowgill.org.uk>  Fri, 15 Nov 2013 14:32:32 +0000

libcsfml (1.6-1) unstable; urgency=low

  * New upstream 1.6 release
  * Bump standards version (no changes)
  * Rename library packages due to soname change
  * Add a -doc package

 -- Christoph Egger <christoph@debian.org>  Sun, 09 May 2010 11:48:39 +0200

libcsfml (1.5-3) unstable; urgency=low

  * Check for CSFML_SYSTEM_FREEBSD alternatively to _LINUX if X11 is
    needed

 -- Christoph Egger <christoph@debian.org>  Sat, 26 Dec 2009 19:06:30 +0100

libcsfml (1.5-2) unstable; urgency=low

  * Change dh_chlean -k to dh_prep and therefor bump debhelper to 7
  * Get rid of strange Make error on systems where topgit is not
    installed (e.g. buildds, chroots)
  * Line Break Depends/Build-Depends
  * Update my Email Address
  * Fix Build on kFreeBSD and GNU/Hurd

 -- Christoph Egger <christoph@debian.org>  Fri, 18 Dec 2009 13:31:53 +0100

libcsfml (1.5-1) unstable; urgency=low

  * New upstream 1.5 release
    * Builds against newer libsfml (Closes: #542982)
    * SONAME bump for libraries
    * Drop debian/gcc4.3.diff (implemented upstream)
    * Refresh debian/correct-destdir
  * Improve descriptions
  * Clean up rules file -- some quirks no longer needed because of
    improvements in upstream buildsystem
  * Bump standards version to 3.8.3 (no changes)
  * remove duplicate priority entries in control
  * Add README.source

 -- Christoph Egger <debian@christoph-egger.org>  Wed, 30 Sep 2009 01:29:32 +0200

libcsfml (1.4-2) unstable; urgency=low

  * should really go to unstable

 -- Christoph Egger <debian@christoph-egger.org>  Sun, 29 Mar 2009 20:47:52 +0200

libcsfml (1.4-1) experimental; urgency=low

  [ Christoph Egger ]
  * New Upstream Version
  * Drop patches now upstream
    * RM: add_destdir_to_makefile.patch
    * RM: fix_soname.patch
  * Convert patch system to topgit
  * Fix problem in upstream's destdir implementation
  * Change my Mailaddress
  * Wrap -dev package's depends

  [ Gonéri Le Bouder ]
  * libcsfml-dev conflicts on/replaces libcsfml1.deb3-dev

 -- Christoph Egger <debian@christoph-egger.org>  Tue, 20 Jan 2009 19:33:08 +0100

libcsfml (1.3-2) unstable; urgency=low

  * Dropping depends on old libsfml packages that should not be there
    the depends automatically pulled in should be enough here

 -- Christoph Egger <Christoph.Egger@gmx.de>  Tue, 09 Sep 2008 11:11:46 +0200

libcsfml (1.3-1) unstable; urgency=low

  * Initial release (Closes: #481665)

 -- Christoph Egger <Christoph.Egger@gmx.de>  Wed, 25 Jun 2008 16:10:19 +0200
